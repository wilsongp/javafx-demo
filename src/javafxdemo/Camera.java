/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxdemo;

import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Greg
 */
public class Camera extends PerspectiveCamera implements IInputHandler
{
    final Xform cameraXform = new Xform();
    final Xform cameraXform2 = new Xform();
    final Xform cameraXform3 = new Xform();
    protected static final double CAMERA_INITIAL_DISTANCE = -2000;
    protected static final double CAMERA_INITIAL_X_ANGLE = 70.0;
    protected static final double CAMERA_INITIAL_Y_ANGLE = 320.0;
    protected static final double CAMERA_NEAR_CLIP = 0.1;
    protected static final double CAMERA_FAR_CLIP = 100000.0;
    
    private static final double CONTROL_MULTIPLIER = 0.1;    
    private static final double SHIFT_MULTIPLIER = 10.0;    
    private static final double MOUSE_SPEED = 0.5;    
    private static final double ROTATION_SPEED = 2.0;    
    private static final double TRACK_SPEED = 0.3;
    
    double mousePosX;
    double mousePosY;
    double mouseOldX;
    double mouseOldY;
    double mouseDeltaX;
    double mouseDeltaY;
    
    public Camera(Group root, boolean fixed){
        super(fixed);
        root.getChildren().add(cameraXform);
        cameraXform.getChildren().add(cameraXform2);
        cameraXform2.getChildren().add(cameraXform3);
        cameraXform3.getChildren().add(this);
 
        super.setNearClip(CAMERA_NEAR_CLIP);
        super.setFarClip(CAMERA_FAR_CLIP);
        super.setTranslateZ(CAMERA_INITIAL_DISTANCE);
    }
    
    @Override
    public void handleKeyboard(Scene scene, final Node root) {
        scene.setOnKeyPressed((KeyEvent event) -> {
            switch (event.getCode()) {
                case Z:
                    cameraXform2.t.setX(0.0);
                    cameraXform2.t.setY(0.0);
                    cameraXform.ry.setAngle(CAMERA_INITIAL_Y_ANGLE);
                    cameraXform.rx.setAngle(CAMERA_INITIAL_X_ANGLE);
                    break;
            }
        });
    }
    
    @Override
    public void handleMouse(Scene scene, final Node root) {
 
        scene.setOnMousePressed((MouseEvent me) -> {
            mousePosX = me.getSceneX();
            mousePosY = me.getSceneY();
            mouseOldX = me.getSceneX();
            mouseOldY = me.getSceneY();
        });
        scene.setOnMouseDragged((MouseEvent me) -> {
            mouseOldX = mousePosX;
            mouseOldY = mousePosY;
            mousePosX = me.getSceneX();
            mousePosY = me.getSceneY();
            mouseDeltaX = (mousePosX - mouseOldX);
            mouseDeltaY = (mousePosY - mouseOldY);
            
            double modifier = 1.0;
            
            if (me.isControlDown()) {
                modifier = CONTROL_MULTIPLIER;
            }
            if (me.isShiftDown()) {
                modifier = SHIFT_MULTIPLIER;
            }
            if (me.isPrimaryButtonDown()) {
                cameraXform.ry.setAngle(cameraXform.ry.getAngle() -
                        mouseDeltaX*MOUSE_SPEED*modifier*ROTATION_SPEED);  //
                cameraXform.rx.setAngle(cameraXform.rx.getAngle() +
                        mouseDeltaY*MOUSE_SPEED*modifier*ROTATION_SPEED);  // -
            }
            else if (me.isSecondaryButtonDown()) {
                double z = super.getTranslateZ();
                double newZ = z + mouseDeltaX*MOUSE_SPEED*modifier;
                super.setTranslateZ(newZ);
            }
            else if (me.isMiddleButtonDown()) {
                cameraXform2.t.setX(cameraXform2.t.getX() +
                        mouseDeltaX*MOUSE_SPEED*modifier*TRACK_SPEED);  // -
                cameraXform2.t.setY(cameraXform2.t.getY() +
                        mouseDeltaY*MOUSE_SPEED*modifier*TRACK_SPEED);  // -
            }
        });
   } 
}
