package javafxdemo;

import javafx.scene.Node;
import javafx.scene.Scene;

/**
 *
 * @author Greg
 */
public interface IInputHandler
{
    public void handleKeyboard(Scene scene, final Node root);
    
    public void handleMouse(Scene scene, final Node root);
}

