package javafxdemo.objects;

import javafx.geometry.Point3D;
import javafx.scene.effect.Light.Point;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafxdemo.Xform;

/**
 * Base object class that represents anything drawn in the world
 * @author Greg
 */
public class Item extends Xform
{
    protected Point3D Vec;
    private final double diameter;
    public static final PhongMaterial redMaterial = new PhongMaterial( Color.DARKRED );
    public static final PhongMaterial whiteMaterial = new PhongMaterial( Color.WHITE );
    public static final PhongMaterial greyMaterial = new PhongMaterial( Color.DARKGREY );

    public Item(double diam){
        Vec = new Point3D(0,0,0);
        diameter = diam;
    }
    
    public Item(double x, double y, double z, double diam)
    {
        super.setTranslate( x, y, z );
        Vec = new Point3D(0,0,0);
        diameter = diam;
    }
    
    public Item(double x, double y, double z, double vx, double vy, double vz, double diam)
    {
        super.setTranslate( x, y, z );
        Vec = new Point3D(vx,vy,vz);
        diameter = diam;
    }
    
    /**
     * Gets distance between the surface of this and given reference Item()
     * 
     * @param ref Item to get distance from
     * @return double - Distance between surface of objects
     */
    public double getDistance( Item ref ){
        return Math.sqrt( 
                Math.pow( t.getX() - ref.t.getX(), 2)
                + Math.pow( t.getY() - ref.t.getY(), 2)
                + Math.pow( t.getZ() - ref.t.getZ(), 2)
            ) - (ref.getDiameter()) - (getDiameter());
    }
    
    public double getAngle( Item ref ){
        return Math.asin( t.getY() - ref.t.getY() / getDistance(ref) );
    }
    
    public double getInclination( Item ref ){
        double dY = ref.t.getY()-t.getY();
        return dY != 0 ? Math.atan(ref.t.getX()-t.getX())/dY : 0;
    }
    
    public String showVec( ){
        return Vec.toString();
    }

    public Point3D getVec()
    {
        return Vec;
    }

    public void setVec( Point3D Vec )
    {
        this.Vec = Vec;
    }
    
    public double getDiameter()
    {
        return diameter;
    }
    
    protected final void setColors(){
        redMaterial.setDiffuseColor( Color.DARKRED );
        redMaterial.setSpecularColor( Color.RED );
        whiteMaterial.setDiffuseColor( Color.WHITE );
        whiteMaterial.setSpecularColor( Color.LIGHTBLUE );
        greyMaterial.setDiffuseColor( Color.DARKGREY );
        greyMaterial.setSpecularColor( Color.GREY );
    }
}
