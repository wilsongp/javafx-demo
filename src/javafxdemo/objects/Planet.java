package javafxdemo.objects;

import javafx.scene.shape.Sphere;
import javafxdemo.Xform;

/**
 * Because I'm not an astronomer  I'm just calling all celestial bodies planets
 * @author Greg
 */
public class Planet extends Item
{
    public Planet( double rads )
    {
        super(rads);
        Xform oxygenXform = new Xform();
        Sphere PlanetSphere = new Sphere(rads);
        PlanetSphere.setMaterial( redMaterial );
        oxygenXform.getChildren().add(PlanetSphere);

        super.getChildren().add(oxygenXform);
    }
    
    public Planet(double x, double y, double z, double rads ){
        super(x, y, z, rads);
        Xform oxygenXform = new Xform();
        Sphere PlanetSphere = new Sphere(rads);
        PlanetSphere.setMaterial( redMaterial );
        oxygenXform.getChildren().add(PlanetSphere);

        super.getChildren().add(oxygenXform);
    }
}
