/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxdemo.objects;

import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafxdemo.Xform;

/**
 *
 * @author Greg
 */
public class Axes extends Xform
{
    private static final double AXIS_LENGTH = 250.0;
    private boolean shown;
    public Axes(Xform parent, boolean show)
    {
        shown = show;
        final PhongMaterial redMaterial = new PhongMaterial();
        redMaterial.setDiffuseColor( Color.DARKRED );
        redMaterial.setSpecularColor( Color.RED );
        
        final PhongMaterial greenMaterial = new PhongMaterial();
        greenMaterial.setDiffuseColor( Color.DARKGREEN );
        greenMaterial.setSpecularColor( Color.GREEN );
        
        final PhongMaterial blueMaterial = new PhongMaterial();
        blueMaterial.setDiffuseColor( Color.DARKBLUE );
        blueMaterial.setSpecularColor( Color.BLUE );
        
        final Box xAxis = new Box(AXIS_LENGTH, 1, 1);
        final Box yAxis = new Box(1, AXIS_LENGTH, 1);
        final Box zAxis = new Box(1, 1, AXIS_LENGTH);
        
        xAxis.setMaterial( redMaterial );
        yAxis.setMaterial( greenMaterial );
        zAxis.setMaterial( blueMaterial );
        
        super.getChildren().addAll( xAxis, yAxis, zAxis );
        super.setVisible( shown );
        parent.getChildren().addAll( this );
    }

    public boolean isShown()
    {
        return shown;
    }

    public void setShown( boolean shown )
    {
        this.shown = shown;
    }
}
