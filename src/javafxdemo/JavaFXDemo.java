package javafxdemo;

import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.DepthTest;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafxdemo.objects.Axes;
import javafxdemo.objects.*;

/**
 * Just a basic program to mess around with JavaFX
 *
 * @author Greg
 */
public class JavaFXDemo extends Application
{

    static final double ANIMATION_SPEED = 0.1;
    static final double UNIVERSAL_SCALE = 0.01;
    static final double EARTH_DIAMETER = 12756.0 * UNIVERSAL_SCALE; // 12756 km 
    static final double MOON_DIAMETER = 3476.0 * UNIVERSAL_SCALE; // 3476 km
    static final double LUNAR_DISTANCE = 363104.0 * UNIVERSAL_SCALE*0.08;

    final Group root = new Group();
    final Xform world = new Xform();
    final Axes Axes = new Axes( world, true );
    final Camera Cam = new Camera( root, true );
    final Planet Earth = new Planet( EARTH_DIAMETER );
    final Planet Moon = new Planet( LUNAR_DISTANCE, 0.0, 0.0, MOON_DIAMETER );
    final Timeline Loop = new Timeline();

    @Override
    public void start( Stage primaryStage )
    {
        root.getChildren().add( world );
        root.setDepthTest( DepthTest.ENABLE );
        Loop.setCycleCount( Timeline.INDEFINITE );

        buildPlanets( world );

        Scene scene = new Scene( root, 1024, 768, true );
        scene.setFill( Color.GREY );
        Cam.handleKeyboard( scene, world );
        Cam.handleMouse( scene, world );

        primaryStage.setTitle( "Molecule Sample Application" );
        primaryStage.setScene( scene );
        primaryStage.show();
        scene.setCamera( Cam );
        System.out.println( "Moon to Earth : " + Moon.getDistance( Earth ) + " | " + Moon.getDiameter() );
        /*
         * Animation Loop using anonymous inner class because it's trivial
         * x = x0 + r sin(θ) cos(φ)
         * y = y0 + r sin(θ) sin(φ)
         * z = z0 + r cos(θ)
         */
        final long startTime = System.nanoTime();
        KeyFrame kf = new KeyFrame(
            Duration.seconds(0.017), // 60 FPS
            (ActionEvent ae) -> {

                double time = (System.currentTimeMillis() - startTime) / 1000000000.0;
                double x = Moon.t.getTx() * Math.cos( time );
                double y = Moon.t.getTy() * Math.sin( time );
                double z = Moon.t.getTz();

                System.out.println("Moon Coords : "+x+", "+y+", "+z);
                Moon.setTranslate( x, y, z );
                if(Moon.getDistance( Earth ) <= 0)
                    Loop.stop();
            }
        );
        
        Loop.getKeyFrames().add( kf );
        Loop.play();

    }

    private void buildPlanets( Xform parent )
    {
        //Moon.setTranslate( 200.0, 50.0, 0.0 );
        parent.getChildren().addAll( Earth );
        parent.getChildren().addAll( Moon );
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX
     * application. main() serves only as fallback in case the
     * application can not be launched through deployment artifacts,
     * e.g., in IDEs with limited FX support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main( String[] args )
    {
        launch( args );
    }

}
